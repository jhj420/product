

.PHONY: proto
proto:
	protoc --go_out=. --micro_out=. ./proto/product/product.proto

.PHONY: build
build: 
	go build -o product *.go

.PHONY: test
test:
	go test -v ./... -cover

.PHONY: docker
docker:
	docker build . -t product:latest
